/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { store } from '../store.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

class MyMap extends connect(store)(PageViewElement) {
  static get properties() {
    return {
      _rawMapMarkers: { type: Array },
      _createdMapMarkers: { type: Array },
      _map: { type: Object },
    }
  }

  static get styles() {
    return [
      SharedStyles,
      css`
        #map {
          height: 100vh;
        }
      `
    ];
  }

  render() {
    return html`
      <section>
        <div id="map"></div>
      </section>
    `;
  }

  constructor() {
    super();

    this._createdMapMarkers = [];
  }

  firstUpdated() {
    this._createMapInitFunction(this.shadowRoot.getElementById('map'));
    this._initMapScript();
  }

  updated(changedProps) {
    if (changedProps.has('_rawMapMarkers') && this._map) {
      this._createMapMarkers()
    }
  }

  _createMapInitFunction(mapDiv) {
    window.initMap = () => {
      this._map = new google.maps.Map(mapDiv, {
        center: { lat: -33.9328, lng: 18.417189 },
        zoom: 10
      });

      this._createMapMarkers();
    };
  }

  _createMapMarkers() {
    if (this._createdMapMarkers) {
      this._createdMapMarkers.map((marker) => marker.setMap(null));
      this._createdMapMarkers = [];
    }

    this._rawMapMarkers.map((marker) => {
      var marker = new google.maps.Marker({
        position: { lat: marker.Lat, lng: marker.Long },
        title: `${marker.Firstname} ${marker.Surname}`
      });
      this._createdMapMarkers.push(marker)
    })

    this._createdMapMarkers.map((marker) => {
      marker.setMap(this._map)
    })
  }

  stateChanged(state) {
    this._rawMapMarkers = state.app.mapMarkers;
  }

  _initMapScript() {
    let script = document.createElement('script');
    script.async = true;
    script.defer = true;
    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAapoveu55Gex7E68PfwoLsM0xelEDTb1I&callback=initMap";
    document.head.appendChild(script)
  }
}

window.customElements.define('my-map', MyMap);
