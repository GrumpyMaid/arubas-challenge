## Hey there! 

### In the essence of time, I decided to go with the PWA-Starter kit. It's a simple template which gets you going quickly.

### What I did.
- You can find the majority of the work I did on the 'my-map.js' page, also the fuzzy search on the 'my-app.js', and some of the redux actions/reducers.
- A small bit of custom styling was applied.
- Bits here and there, not worthy to mention.

#### SETUP
- Clone the repo
- run 'npm install -g polymer-cli' which you can use to run a localhost site, or use whichever you prefer. (Node.JS 8.11 or higher required)
- run 'npm install' in the home directory of the project to grab all its dependencies
- run 'polymer serve' in the home directory to create a localhosted site of the project.
- Visit the link
- ???
- profit.

#### BUILDS
- You can create various builds of the project, again with the polymer-cli. (You'll need it to run the build)
- run 'npm build' which will create 3 versions of the project (defined in the polymer.json file), serve whichever you need or use differential serving with prpl-server.
- This includes minifying the html, css and js.
- And transforming the js into an older more compatible version for older browers. (If that specific ES5 build is used)

#### Styling
- I didn't end up having enough time to work on the styling, it's lacking in many parts. Apologies.







[![Built with pwa–starter–kit](https://img.shields.io/badge/built_with-pwa–starter–kit_-blue.svg)](https://github.com/Polymer/pwa-starter-kit "Built with pwa–starter–kit")
[![Build status](https://api.travis-ci.org/Polymer/pwa-starter-kit.svg?branch=template-responsive-drawer-layout)](https://travis-ci.org/Polymer/pwa-starter-kit)

# PWA Starter Kit -- `template-responsive-drawer-layout`

This sample app is a starting point for building PWAs. Out of the box, the template
gives you the following features:
- all the PWA goodness (manifest, service worker)
- a responsive layout
- application theming
- example of using Redux for state management
- offline UI
- simple routing solution
- fast time-to-interactive and first-paint through the PRPL pattern
- easy deployment to prpl-server or static hosting
- unit and integrating testing starting points
- documentation about other advanced patterns.

This template is very similar to the `master` template, in the sense that it keeps both Redux for state management, and all of the UI elements. The main difference is that the wide screen layout displays a persistent `app-drawer`, inline with the content.

### 📖 Head over to the [documentation site](https://pwa-starter-kit.polymer-project.org/) for more details or check out [how to get started](https://pwa-starter-kit.polymer-project.org/setup/)!

![pwa-starter-kit screenshot](https://user-images.githubusercontent.com/116360/39718020-dd60403e-51e9-11e8-9384-e019a6775841.png)

## TODOs

- [x] Setup Safari testing on Travis.
- [x] Deploy all templates as demos.
- [ ] Update to latest [Material Web Components](https://github.com/material-components/material-components-web-components).
